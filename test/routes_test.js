const chai = require('chai');
const expect = chai.expect;

// const {expect} = require('chai')

const http = require('chai-http');
chai.use(http);

describe("API Test Suite", () => {
	it("Test API GET People are running", () =>{
		chai.request('http://localhost:5001/people').get('/people').end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it("Test API GET people returns 200", (done) =>{
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		})
	})

	it("TEST API POST Person returns 400 if no person name", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Jason",
			age: "28"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	});

/* activity 2 */

	it("[1] Check if Person endpoint is running", () => {
		chai.request('http://localhost:5001/person').post('/person').end((err, res) => {
			expect(res).to.not.equal(undefined);
		});
	});

	it("[2] Test API post person returns 400 if no Alias", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "Jason",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	});

	it("[3] Test API post person returns 400 if no AGE", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "Jason",
			alias: "j"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	});
})