function factorial(n){
	if(typeof n !== 'number') return undefined;
	if(n < 0 ) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	return n * factorial(n - 1);
}

function odd_or_even(n) {
	if(n%2 == 0) return true;
	if(typeof n !== 'number') return undefined;
		else return false;
}

// activity 1

function div_check(n) {
	if(n%7 === 0 || n%5 === 0) return true;
	if(typeof n !== 'number') return undefined;
	else return false
};

// end of activity 1

const names = {
	"Boba": {
		"name": "Boba Fett",
		"age": 50
	},
	"Anakin": {
		"name": "Anakin Skywalker",
		"age": "65"
	}
}

module.exports = {
	factorial: factorial,
	odd_or_even:odd_or_even,
	div_check: div_check,
	names: names
}


